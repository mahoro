Gem::Specification.new do |s|
  s.name = %q{mahoro}
  s.version = (ENV['VERSION'] || '0.5').dup
  s.homepage = "https://yhbt.net/mahoro/README"
  s.authors = [ "Shu-yu Guo", "Eric Wong" ]
  s.description = %{
An interface to libmagic to determine file types using "magic" numbers.
This can be used in place of calling the file(1) command in Ruby scripts.
Shu-yu Guo is the original author but all maintenance is handled by
Eric Wong nowadays.

source: https://yhbt.net/mahoro.git/
documentation: https://yhbt.net/mahoro/
  }.strip
  s.email = %q{normalperson@yhbt.net}
  s.files = `git ls-files`.split(/\n/)
  s.rubyforge_project = %q{mahoro}
  s.summary = 'An interface to libmagic'
  s.extensions = %w(ext/mahoro/extconf.rb)
  s.extra_rdoc_files = %w(ext/mahoro/mahoro.c)
  s.rdoc_options = %w(--exclude \.o --exclude \.so)

  s.license = "Public Domain"
end
