require 'mkmf'

dir_config('magic')
have_library('magic', 'magic_open')
have_func('rb_thread_call_without_gvl')
have_func('rb_thread_blocking_region')
create_makefile('mahoro')

# arch-tag: extconf
