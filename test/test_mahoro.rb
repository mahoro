#!/usr/bin/env ruby

require 'test/unit'
require 'mahoro'
require 'mahoro/thread_safe'
require 'pathname'

class MahoroTestCase < Test::Unit::TestCase
	C_FILE = "#{File.dirname(__FILE__)}/../ext/mahoro/mahoro.c"

	def setup
		@m = Mahoro.new
	end

	# Different versions of libmagic may generate different values.
	# So far, we have seen:
	# - ASCII C program text
	# - C source, ASCII text
	def assert_c_text(buf)
		assert_match(/\bC\b/, buf, "contains the letter C")
		assert_match(/\s/, buf, "contains spaces")
		assert_match(/(?:source|text)/, buf, "is source or text")
	end

	def test_pathname
		@m.flags = Mahoro::NONE
                pn = Pathname.new(C_FILE)
		assert_c_text(@m.file(pn))
	end

	def test_file
		@m.flags = Mahoro::NONE
		assert_c_text(@m.file(C_FILE))
	end

	def test_mime_file
		@m.flags = Mahoro::MIME
		assert({
		  'text/x-c; charset=us-ascii' => true,
		  'text/x-c charset=us-ascii' => true
		}.include?(@m.file(C_FILE)))
	end

	def test_null_byte_in_path
		assert_raises(ArgumentError) { @m.file("#{C_FILE}\0foo") }
	end

	def test_buffer
		@m.flags = Mahoro::NONE
		assert_c_text(@m.buffer(File.read(C_FILE)))
	end

	def test_buffer_string_convert
		tmp = File.read(C_FILE)
		buf = Struct.new(:to_str).new(tmp)
		assert_c_text(@m.buffer(buf))
	end

	def test_buffer_invalid
		@m.flags = Mahoro::NONE
		assert_raises(TypeError) { @m.buffer @m }
	end

	def test_mime_buffer
		@m.flags = Mahoro::MIME
		assert({
		  'text/x-c; charset=us-ascii' => true,
		  'text/x-c charset=us-ascii' => true
		}.include?(@m.buffer(File.read(C_FILE))))
	end

	def test_valid
		assert(@m.valid?, 'Default database was not valid.')
	end

	def test_valid_with_null
		assert_raises(ArgumentError) { @m.valid? "#{C_FILE}\0" }
	end

	def test_compile
		File.open(__FILE__) do |fp|
			fp.flock File::LOCK_EX
			assert Mahoro.compile("magic.sample")
			assert_nothing_raised do
				File.unlink("magic.sample.mgc")
			end
		end
	end

	def test_compile_bad
		assert_raises(ArgumentError) do
			Mahoro.compile "magic.sample\0"
		end
	end

	def test_thread_safe
		before = @m.method(:file)
		@m.extend(Mahoro::ThreadSafe)
		@m.flags = Mahoro::NONE
		assert_c_text(@m.file(C_FILE))
		assert_not_equal(before.object_id, @m.method(:file).object_id)
	end
end

# arch-tag: test
